/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santosoftvw.intellichoice.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.santosoftvw.car.CarFeatureWeight;
import com.santosoftvw.car.FeaturesOfParticularSeries;


public class RecommendationDao {
    
    public Object[][] getCarRecommendation(double averageQuality){
        Object[][] carFeatures = new Object[10][10];
        String carRecommendationSql = "select f.Model_no,f.Color,f.Sit_capacity,f.Fuel_tank_capacity,"
                + "f.Transmission,f.Body_style,f.Millage,f.Engine_performance,f.Price from features_of_particular_series f,"
                + "car_features_weight c where f.Model_no=c.Model_no and c.average_quality > ?";
        Connection con = DBConnection.getDBConnection();
        if(con!=null){
            try {
                PreparedStatement pstmt = con.prepareStatement(carRecommendationSql);
                pstmt.setDouble(1, averageQuality);
                ResultSet rst = pstmt.executeQuery();
                int i = 0;
                while(rst.next()){
                    System.out.println(rst.getString("Model_no"));
                    carFeatures[i][0] = rst.getString("Model_no");
                    carFeatures[i][1] = rst.getString("Color");
                    carFeatures[i][2] = rst.getString("Sit_capacity");
                    carFeatures[i][3] = rst.getString("Fuel_tank_capacity");
                    carFeatures[i][4] = rst.getString("Transmission");
                    carFeatures[i][5] = rst.getString("Body_style");
                    carFeatures[i][6] = rst.getString("Millage");
                    carFeatures[i][7] = rst.getString("Engine_performance");
                    carFeatures[i][8] = rst.getString("Price");
                    i++;
                  }                
            }
             catch (SQLException ex) {
                Logger.getLogger(RecommendationDao.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }    
     return carFeatures;
    }
    
    public void insertNewCarFeature(CarFeatureWeight carFeatureWeight){
    	String insertNewCarFeatureStatement = "insert into car_features_weight values(?,?,?,?,?)";
    	Connection con = DBConnection.getDBConnection();
        if(con!=null){
        	try{
        		PreparedStatement pstmt = con.prepareStatement(insertNewCarFeatureStatement);
                pstmt.setString(1, carFeatureWeight.getModelNo());
        		pstmt.setDouble(2, carFeatureWeight.getPriceRange());
        		pstmt.setDouble(3, carFeatureWeight.getMilleage());
        		pstmt.setDouble(4, carFeatureWeight.getEnginePerformance());
        		pstmt.setDouble(5, carFeatureWeight.getAverageQuality());
        		pstmt.executeUpdate();
        	}catch(SQLException ex){
        		Logger.getLogger(RecommendationDao.class.getName()).log(Level.SEVERE, null, ex);
        	}
        }
    }
    
    public void insertFeaturesOfParticularSeries(FeaturesOfParticularSeries featuresOfParticularSeries){
    	String insertNewfeaturesOfParticularSeriesStatement = "insert into features_of_particular_series values(?,?,?,?,?,?,?,?,?,?,?)";
    	Connection con = DBConnection.getDBConnection();
        if(con!=null){
        	try{
        		PreparedStatement pstmt = con.prepareStatement(insertNewfeaturesOfParticularSeriesStatement);
                pstmt.setString(1, featuresOfParticularSeries.getColor());
        		pstmt.setString(2, featuresOfParticularSeries.getModel_no());
        		pstmt.setString(3, featuresOfParticularSeries.getTransmission());
        		pstmt.setString(4, featuresOfParticularSeries.getEngine_type());
        		pstmt.setString(5, featuresOfParticularSeries.getFuel_tank_capacity());
        		pstmt.setString(6, featuresOfParticularSeries.getDrive_type());
        		pstmt.setString(7, featuresOfParticularSeries.getSit_capacity());
        		pstmt.setString(8, featuresOfParticularSeries.getBody_style());
        		pstmt.setString(9, featuresOfParticularSeries.getPrice());
        		pstmt.setString(10, featuresOfParticularSeries.getMillage());
        		pstmt.setString(11, featuresOfParticularSeries.getEngine_performance());
        		pstmt.executeUpdate();
        	}catch(SQLException ex){
        		Logger.getLogger(RecommendationDao.class.getName()).log(Level.SEVERE, null, ex);
        	}
        }
    }
}
