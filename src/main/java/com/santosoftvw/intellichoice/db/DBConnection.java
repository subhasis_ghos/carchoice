/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santosoftvw.intellichoice.db;

import java.sql.*;
import java.sql.SQLException;

public class DBConnection {
    
    private static Connection dbConnection = null;
   // private static boolean isDbConnected=false;
    
    public static Connection getDBConnection(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            dbConnection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/carchoice","root","qwerty");
        }
        catch(ClassNotFoundException ce){
            ce.printStackTrace();
        }
        catch(SQLException se){
            se.printStackTrace();
        }
        return dbConnection;
        
    }
    
    
}