package com.santosoftvw.car;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BackwardPropagationAlgorithm {
	
	private NeuronInput neuronInput;
        
        private Queue[] hiddenLayerNeurons;
        
        Queue[] hiddenLayerWeight;
        
        private List outputLayerInput;
        
        private int inputSize = 0 ;
        private Object outputError;
        private double target = 0.0;
        public double finalOutput;
        private double[] wtLayer;
        
        public BackwardPropagationAlgorithm(double input1,double input2,double input3,double avgInput){
            outputLayerInput = new ArrayList();
            neuronInput = new NeuronInput();
            neuronInput.setInput(input1);
            neuronInput.setInput(input2);
            neuronInput.setInput(input3);
            neuronInput.setOutput(avgInput);
            inputSize = neuronInput.getInput().size();
            target = neuronInput.getTarget();
        }
        
        protected void addHiddenInput(){           
           hiddenLayerNeurons = new PriorityQueue[inputSize];
           for(int i=0;i<inputSize;i++){
               hiddenLayerNeurons[i] = new PriorityQueue();
               for(int j=0;j<inputSize;j++){
                   double[] weights = new double[inputSize];
                  
                   weights = generateRandomWeight();
                   hiddenLayerNeurons[i].add(weights[j]);
               }
           }           
        }
        
        protected void addOutputLayerInput(){
            double[] randNumWt = generateRandomWeight();
            for(int j=0;j<inputSize;j++){
                outputLayerInput.add(randNumWt[j]);
            }
        }
        
        private double[] generateRandomWeight(){
            double[] randomWeight = new double[inputSize];
            double randNum = 0.0;
            double randNumIn = 0.0;
            for(int i=0;i<inputSize;i++){
                randNumIn = Math.random();
                randNum += randNumIn;
                if(randNum<1){
                    randomWeight[i] = randNumIn;
                    if(i==inputSize-1){
                        break;
                    }
                }
                else{
                    randNum -= randNumIn;
                    continue;
                }
            }
            return randomWeight;
        }
        
        protected double[] getWeightOutput()
        {
            double weightInput = 0;
            double weightOutput[]= new double[inputSize];
            double input1 = 0;
            double input2 = 0;
            for(int i=0;i<inputSize;i++)
                {
                  for(int j=0;j<inputSize;j++)
                     {
                        input1 = neuronInput.getInput().get(j);
                        //String input3 = String.valueOf(hiddenLayerNeurons[i].get(j));
                        input2 = (Double)hiddenLayerNeurons[i].peek();//.get(j);
                
                        weightInput += input1*input2;
                                
                      }
                            weightOutput[i] = 1/(1+Math.exp(-weightInput));
                            System.out.println(weightOutput[i]);
                }
            return weightOutput;
		
        }
        
        protected double getFinalOutput()
        {
            double finalInput=0;
            double finalInitialInput=0;
            for(int i=0;i<inputSize;i++)
            {
                //String inputFnl = String.valueOf(outputLayerInput.get(i));
                finalInitialInput= (Double)outputLayerInput.get(i);
                finalInput+=finalInitialInput;
             }
                finalOutput = 1/(1+Math.exp(-finalInput));
                return finalOutput;
                        
         }
        
      
        public double getOutputError(){
            double outputError = 0.0;
            double output = getFinalOutput();
            outputError= output*(1-output)*(target-output);            
            return outputError;
        }
        
        public double[] getOutputLayerWeight(){
            double wtOutput[] = getWeightOutput();
            wtLayer = new double[inputSize];
            double outputError = getOutputError();
            int i = 0;
            for(double wt : wtOutput){
                //String str = String.valueOf(outputLayerInput.get(i));
                wtLayer[i]=wt+(outputError*(Double)outputLayerInput.get(i));
                i++;
            }
            return wtLayer;
        }
        
        public double[] getErrorForHiddenLayer(){
            double[] hiddenLayerErrors = new double[inputSize];
            double[] oLayrWt = getOutputLayerWeight();
            int i=0;
            for(double wt : oLayrWt){
                hiddenLayerErrors[i] = getOutputError() * wt;
                i++;
            }
            return hiddenLayerErrors;
        }
        
        //public Queue[] getHiddenLayerWeight(){
        public boolean getHiddenLayerWeight(){
            //Queue[] hiddenLayerWeights = new PriorityQueue[inputSize];
            boolean result = false;
            double[] hiddenLayerErrors = getErrorForHiddenLayer();
            List inputList = neuronInput.getInput();
            for(int i=0; i<inputSize; i++){
                //hiddenLayerWeights[i] = new PriorityQueue();
                for(int j=0; j<hiddenLayerNeurons.length; j++){
                    //String strInput = String.valueOf(inputList.get(i));
                    double neuronInput = (Double)inputList.get(i);
                    
                    double hiddenInput = (Double)hiddenLayerNeurons[i].poll();//.get(j);
                    
                    //hiddenLayerWeights[i].add(hiddenInput+(hiddenLayerErrors[i]*neuronInput));
                    hiddenLayerNeurons[i].add(hiddenInput+(hiddenLayerErrors[i]*neuronInput));
                }
            }
            return result=true;
            //return hiddenLayerWeights;
        } 
        
	public NeuronInput backwardPropagate() {
		
            //hiddenLayerWeight = getHiddenLayerWeight();
            boolean result = getHiddenLayerWeight();
            double difference = 0.0;
            if(result==true){
                if(target>finalOutput){
                	
                    difference = target - finalOutput;                    
                }
                else{
                    difference = finalOutput-target;
                }
                if(difference>0.1){
                    //backwardPropagate();
                }
                for(int i=0; i<wtLayer.length; i++){
                    //System.out.println(wtLayer[i]);
                }
                System.out.println("Final Output: "+finalOutput);
            }
           
                         
		return neuronInput;
	}
        
        public static void main(String args[]){
            BackwardPropagationAlgorithm b = new BackwardPropagationAlgorithm(0.35,0.45,0.2,0.33);
            
            b.addHiddenInput();
            b.addOutputLayerInput();
            for(int i=0;i<300; i++){
                b.backwardPropagate();
            }
            
        }

}
