package com.santosoftvw.car;

public class FeaturesOfParticularSeries {

	private String Color;
	private String Model_no;
	private String Transmission;
	private String Engine_type;
	private String Fuel_tank_capacity;
	private String Drive_type;
	private String Sit_capacity;
	private String Body_style;
	private String Price;
	private String Millage;
	private String Engine_performance;
	
	public FeaturesOfParticularSeries(){
		
	}
	
	public String getColor() {
		return Color;
	}
	public void setColor(String color) {
		Color = color;
	}
	public String getModel_no() {
		return Model_no;
	}
	public void setModel_no(String model_no) {
		Model_no = model_no;
	}
	public String getTransmission() {
		return Transmission;
	}
	public void setTransmission(String transmission) {
		Transmission = transmission;
	}
	public String getEngine_type() {
		return Engine_type;
	}
	public void setEngine_type(String engine_type) {
		Engine_type = engine_type;
	}
	public String getFuel_tank_capacity() {
		return Fuel_tank_capacity;
	}
	public void setFuel_tank_capacity(String fuel_tank_capacity) {
		Fuel_tank_capacity = fuel_tank_capacity;
	}
	public String getDrive_type() {
		return Drive_type;
	}
	public void setDrive_type(String drive_type) {
		Drive_type = drive_type;
	}
	public String getSit_capacity() {
		return Sit_capacity;
	}
	public void setSit_capacity(String sit_capacity) {
		Sit_capacity = sit_capacity;
	}
	public String getBody_style() {
		return Body_style;
	}
	public void setBody_style(String body_style) {
		Body_style = body_style;
	}
	public String getPrice() {
		return Price;
	}
	public void setPrice(String price) {
		Price = price;
	}
	public String getMillage() {
		return Millage;
	}
	public void setMillage(String millage) {
		Millage = millage;
	}
	public String getEngine_performance() {
		return Engine_performance;
	}
	public void setEngine_performance(String engine_performance) {
		Engine_performance = engine_performance;
	}
	

}
