/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santosoftvw.car;

import java.awt.Dimension;
import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

/**
 *
 * @author susmeta
 */
public class IntelligentClientApplet extends JApplet {

    /**
     * Initialization method that will be called after the applet is loaded
     * into the browser.
     */
    public void init() {
        // TODO start asynchronous download of heavy resources
        AdministrationInterface administrationInterface = new  AdministrationInterface();
    	CarInterface carInterface = new CarInterface();
    	ConfigurationInterface configurationInterface = new ConfigurationInterface();
        //RecommendationUI recommendationUI = new RecommendationUI();
        //recommendationUI.init();
	    JFrame jFrame=new JFrame();
	    jFrame.setTitle("Intelichoice");
	    jFrame.getToolkit();
	    Dimension size = jFrame.getToolkit().getScreenSize();
	    jFrame.setBounds(size.width/3,size.height/3,400,300);        
	    jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    JTabbedPane tabbedPane = new JTabbedPane();
	    tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
	    tabbedPane.addTab("User", carInterface);
	    tabbedPane.addTab("Adminstration page", administrationInterface);
	    
	    tabbedPane.addTab("Car Configuration", configurationInterface);
            //tabbedPane.addTab("Recommendation", recommendationUI);
	    jFrame.add(tabbedPane);
	    jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    jFrame.setVisible(true);
	    jFrame.setResizable(false);
    }
    // TODO overwrite start(), stop() and destroy() methods
}
