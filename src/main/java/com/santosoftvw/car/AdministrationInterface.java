package com.santosoftvw.car;

import com.santosoftvw.intellichoice.db.RecommendationDao;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class AdministrationInterface extends JPanel{
	
	private static final long serialVersionUID = 1L;

	public AdministrationInterface()
	{
		
		setLayout(new BorderLayout());
                
		
		JLabel title = new JLabel("Please Input Your Data set:");
		JPanel titlePanel= new JPanel(new GridLayout(1,1));
		titlePanel.add(title);
		
        JLabel model=new JLabel("Model No");		
		JLabel weight1=new JLabel("Weight For Price Range");		
		JLabel weight2=new JLabel("Weight For Milage");
		JLabel weight3=new JLabel("Weight For Engine Performence");
		JPanel weightPanel= new JPanel(new GridLayout(4,0));
		weightPanel.add(model);
        weightPanel.add(weight1);
		weightPanel.add(weight2);
		weightPanel.add(weight3);
		
		
		final JTextField modelInput =new JTextField(10);
		final JTextField priceWeight =new JTextField(10);
		final JTextField milageWeight =new JTextField(10);		
		final JTextField enginePerformanceWeight =new JTextField(10);
		JPanel inputPanel= new JPanel(new GridLayout(4,0));
		inputPanel.add(modelInput);
        inputPanel.add(priceWeight);
		inputPanel.add(milageWeight);
		inputPanel.add(enginePerformanceWeight);
		
		
		JButton submitButton= new JButton("Submit");
		final CarFeatureWeight carFeatureWeight = new CarFeatureWeight();
		final RecommendationDao recommendationDao = new RecommendationDao();
		submitButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				carFeatureWeight.setModelNo(modelInput.getText());
				carFeatureWeight.setPriceRange(Double.valueOf(priceWeight.getText()));
				carFeatureWeight.setMilleage(Double.valueOf(milageWeight.getText()));
				carFeatureWeight.setEnginePerformance(Double.valueOf(enginePerformanceWeight.getText()));
				carFeatureWeight.setAverageQuality(((Double.valueOf(priceWeight.getText())+
						Double.valueOf(milageWeight.getText())+Double.valueOf(enginePerformanceWeight.getText())))/3);
				recommendationDao.insertNewCarFeature(carFeatureWeight);
			}
			
		});
		JButton clearButton= new JButton("Clear");
		clearButton.addActionListener(new ActionListener(){
	    	   public void actionPerformed(ActionEvent ae){
	    		   {
	    			   modelInput.setText(null);
	    			   priceWeight.setText(null);
	    			   milageWeight.setText(null);
	    			   enginePerformanceWeight.setText(null);
	    		   }
	       	}
	       }
	       );
	
	       
		JButton cancelButton= new JButton("Cancel");
		JPanel buttonPanel= new JPanel(new GridLayout(1,0));
		buttonPanel.add(submitButton);
		buttonPanel.add(clearButton);
		buttonPanel.add(cancelButton);
		
		add(titlePanel,BorderLayout.NORTH);
        add(weightPanel,BorderLayout.WEST);
        add(inputPanel,BorderLayout.EAST);
        add(buttonPanel,BorderLayout.SOUTH);
		
	}
	
	public static void main(String[] args) {
			
	}

}
