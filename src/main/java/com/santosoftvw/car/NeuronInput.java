package com.santosoftvw.car;

import java.util.ArrayList;
import java.util.List;

public class NeuronInput {
        
        private List<Double> input = new ArrayList<Double>();
	private double target = 0.0;
        private List<Double> output = new ArrayList<Double>();
        
	public NeuronInput(){
		
	}
	
	public List<Double> getInput(){
            return this.input;
        }
        
        public void setInput(Double inputCriteriaWt){
            if((inputCriteriaWt<1) && (inputCriteriaWt>0)){
                input.add(inputCriteriaWt);
            }
            else{
                
            }
        }
        
        
	public List<Double> getOutput(){
            return this.output;
        }
        
        public void setOutput(Double targetOutput){
            if((targetOutput<1) && (targetOutput>0)){
                output.add(targetOutput);
            }
            else{
                
            }
            for(double tgt : output){
                target += tgt;
            }
            target = target/output.size();           
        }
        
        public double getTarget(){
            return this.target;
        }

}
