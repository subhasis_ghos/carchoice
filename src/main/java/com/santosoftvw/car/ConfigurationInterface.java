package com.santosoftvw.car;

import com.santosoftvw.intellichoice.db.RecommendationDao;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ConfigurationInterface extends JPanel {

	

	private static final long serialVersionUID = 1L;

	public ConfigurationInterface()
	{		
		setLayout(new BorderLayout());
		featuring();
	}
	public void featuring(){
                
		
		JLabel title = new JLabel("Please Input Your Car Features:");
		JPanel titlePanel= new JPanel(new GridLayout(1,0));
		titlePanel.add(title);
		
		JLabel model=new JLabel("Model No");
		JLabel Transmission=new JLabel("Transmission");
		JLabel Color=new JLabel("color");
		JLabel BodyStyle=new JLabel("Body Style");
		JLabel DriveType=new JLabel("Drive Type");
		JLabel SitCapacity=new JLabel("Sit Capacity");
		JLabel EngineType=new JLabel("engine Type");
		JLabel EnginePerformance=new JLabel("Engine Performance");
		JLabel FuelTankCapacity=new JLabel("Fuel Tank Capacity");
		JLabel Milage=new JLabel("Milage");
		JLabel Price=new JLabel("Price");
		//JLabel Photo=new JLabel("Photo");
        
		JPanel FeatureLabelPanel= new JPanel(new GridLayout(13,0));
		FeatureLabelPanel.add(model);
		FeatureLabelPanel.add(Transmission);
		FeatureLabelPanel.add(Color);
		FeatureLabelPanel.add(BodyStyle);
		FeatureLabelPanel.add(DriveType);
		FeatureLabelPanel.add(SitCapacity);
		FeatureLabelPanel.add(EngineType);
		FeatureLabelPanel.add(EnginePerformance);
		FeatureLabelPanel.add(FuelTankCapacity);
		FeatureLabelPanel.add(Milage);
		FeatureLabelPanel.add(Price);
		//FeatureLabelPanel.add(Photo);
		
		/*JLabel fake1 =new JLabel(" ");
		JLabel fake2 =new JLabel(" ");
		JLabel fake3 =new JLabel(" ");
		JLabel fake4 =new JLabel(" ");
		JLabel fake5 =new JLabel(" ");
		JLabel fake6 =new JLabel(" ");
		JLabel fake7 =new JLabel(" ");
		JLabel fake8 =new JLabel(" ");
		JLabel fake9 =new JLabel(" ");
		JLabel fake10 =new JLabel(" ");
		JLabel fake11 =new JLabel(" ");
		JLabel fake12 =new JLabel(" ");
        
		JPanel FakeLabelPanel= new JPanel(new GridLayout(13,0));
		FakeLabelPanel.add(fake1);
		FakeLabelPanel.add(fake2);
		FakeLabelPanel.add(fake3);
		FakeLabelPanel.add(fake4);
		FakeLabelPanel.add(fake5);
		FakeLabelPanel.add(fake6);
		FakeLabelPanel.add(fake7);
		FakeLabelPanel.add(fake8);
		FakeLabelPanel.add(fake9);
		FakeLabelPanel.add(fake10);
		FakeLabelPanel.add(fake11);
		FakeLabelPanel.add(fake12);*/
		
		
        final JTextField modelInput =new JTextField(10);
		final JTextField TransmissionInput =new JTextField(10);
		final JTextField ColorInput =new JTextField(10);
		final JTextField BodyStyleInput =new JTextField(10);
		final JTextField DriveTypeInput =new JTextField(10);
		final JTextField SitCapacityInput =new JTextField(10);
		final JTextField EngineTypeInput =new JTextField(10);
		final JTextField EnginePerformanceInput =new JTextField(10);
		final JTextField FuelTankCapacityInput =new JTextField(10);
		final JTextField MilageInput =new JTextField(10);
		final JTextField PriceInput =new JTextField(10);
		//final JTextField PhotoInput =new JTextField(10);

		JPanel featureInputPanel= new JPanel(new GridLayout(13,0));
		featureInputPanel.add(modelInput);
		featureInputPanel.add(TransmissionInput);
		featureInputPanel.add(ColorInput);
		featureInputPanel.add(BodyStyleInput);
		featureInputPanel.add(DriveTypeInput);
		featureInputPanel.add(SitCapacityInput);
		featureInputPanel.add(EngineTypeInput);
		featureInputPanel.add(EnginePerformanceInput);
		featureInputPanel.add(FuelTankCapacityInput);
		featureInputPanel.add(MilageInput);
		featureInputPanel.add(PriceInput);
		//featureInputPanel.add(PhotoInput);

		//JLabel fakelbl1= new JLabel(" ");
		JButton submitButton= new JButton("Submit");
		final FeaturesOfParticularSeries featuresOfParticularSeries = new FeaturesOfParticularSeries();
		final RecommendationDao recommendationDao = new RecommendationDao();
		submitButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				featuresOfParticularSeries.setModel_no(modelInput.getText());
				featuresOfParticularSeries.setTransmission(TransmissionInput.getText());
				featuresOfParticularSeries.setColor(ColorInput.getText());
				featuresOfParticularSeries.setBody_style(BodyStyleInput.getText());
				featuresOfParticularSeries.setDrive_type(DriveTypeInput.getText());
				featuresOfParticularSeries.setSit_capacity(SitCapacityInput.getText());
				featuresOfParticularSeries.setEngine_type(EngineTypeInput.getText());
				featuresOfParticularSeries.setEngine_performance(EnginePerformanceInput.getText());
				featuresOfParticularSeries.setFuel_tank_capacity(FuelTankCapacityInput.getText());
				featuresOfParticularSeries.setMillage(MilageInput.getText());
				featuresOfParticularSeries.setPrice(PriceInput.getText());
				recommendationDao.insertFeaturesOfParticularSeries(featuresOfParticularSeries);
			}
			
		});
		//JLabel fakelbl2= new JLabel(" ");
		JButton clearButton= new JButton("Clear");
		clearButton.addActionListener(new ActionListener(){
	    	   public void actionPerformed(ActionEvent ae){
	    		   {
	    			   modelInput.setText(null);
	    			   TransmissionInput.setText(null);
	    			   ColorInput.setText(null);
	    			   BodyStyleInput.setText(null);
	    			   DriveTypeInput.setText(null);
	    			   SitCapacityInput.setText(null);
	    			   EngineTypeInput.setText(null);
	    			   EnginePerformanceInput.setText(null);
	    			   FuelTankCapacityInput.setText(null);
	    			   MilageInput.setText(null);
	    			   PriceInput.setText(null);
	    	//		   PhotoInput.setText(null);
	    		   }
	       	}
	       }
	       );
		JLabel fake13= new JLabel(" ");
		JLabel fake14= new JLabel(" ");
		JLabel fake15= new JLabel(" ");
//		JButton cancelButton= new JButton("Cancel");
		JPanel buttonPanel= new JPanel(new GridLayout(1,0));
		buttonPanel.add(fake13);
		buttonPanel.add(submitButton);
		buttonPanel.add(fake14);
		buttonPanel.add(clearButton);
		buttonPanel.add(fake15);
		//buttonPanel.add(cancelButton);
		
		add(titlePanel,BorderLayout.NORTH);
		//add(FakeLabelPanel,BorderLayout.WEST);
	    add(FeatureLabelPanel,BorderLayout.CENTER);
	    add(featureInputPanel,BorderLayout.EAST);
	    add(buttonPanel,BorderLayout.SOUTH);

	}	
	
	public static void main(String[] args) {
		
	}

}
