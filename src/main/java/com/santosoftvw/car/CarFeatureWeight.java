package com.santosoftvw.car;

public class CarFeatureWeight {
	
	private String modelNo;
	private double priceRange;
	private double milleage;
	private double enginePerformance;
	private double averageQuality;
	
	public CarFeatureWeight(){
		
	}
	
	public String getModelNo() {
		return modelNo;
	}
	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}
	public double getPriceRange() {
		return priceRange;
	}
	public void setPriceRange(double priceRange) {
		this.priceRange = priceRange;
	}
	public double getMilleage() {
		return milleage;
	}
	public void setMilleage(double milleage) {
		this.milleage = milleage;
	}
	public double getEnginePerformance() {
		return enginePerformance;
	}
	public void setEnginePerformance(double enginePerformance) {
		this.enginePerformance = enginePerformance;
	}
	public double getAverageQuality() {
		return averageQuality;
	}
	public void setAverageQuality(double averageQuality) {
		this.averageQuality = averageQuality;
	}

}
