package com.santosoftvw.car;

import com.santosoftvw.intellichoice.db.RecommendationDao;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;

public class CarInterface extends JPanel{
								
	private static final long serialVersionUID = 1L;

public CarInterface()
   {
        setLayout(new BorderLayout());
        builtGui();
   }
      
   public void builtGui()           
   {
      
       JPanel reqPanel = new JPanel(new GridLayout(1,1));
       JLabel request = new JLabel("Please Select your requirement:");
       reqPanel.add(request);
    
       JLabel featureName1 = new JLabel("Price");
       final JTextField txtPrice = new JTextField(10);
       /*
       JSlider priceSlider = new JSlider();
       priceSlider.setMajorTickSpacing(10);
       priceSlider.setPaintTicks(true);
       */
       JLabel featureName2 = new JLabel("Milage");
       final JTextField txtMilage = new JTextField(10);
       /*
       JSlider milageSlider = new JSlider();
       milageSlider.setMajorTickSpacing(10);
       milageSlider.setPaintTicks(true);
       */
       JLabel featureName3 = new JLabel("Engine Performance");
       final JTextField txtEnginePerformance = new JTextField(10);
       /*
       JSlider enginePerformanceSlider = new JSlider();
       enginePerformanceSlider.setMajorTickSpacing(10);
       enginePerformanceSlider.setPaintTicks(true);
        */
        
       JPanel featurePanel1 = new JPanel(new GridLayout(3,0));
       featurePanel1.add(featureName1);
       featurePanel1.add(featureName2);
       featurePanel1.add(featureName3);
       
       JPanel featurePanel2 = new JPanel(new GridLayout(3,0));    
       featurePanel2.add(txtPrice);
       featurePanel2.add(txtMilage);
       featurePanel2.add(txtEnginePerformance);
                       
       JPanel btnPanel = new JPanel(new GridLayout(1,0));
       JLabel blank1 = new JLabel(" ");
       JButton submit= new JButton("Submit");
       submit.addActionListener(new ActionListener(){
       public void actionPerformed(ActionEvent ae){
    		   {
    			    System.out.print("He He");
                            double input1 = Double.parseDouble(txtPrice.getText());
                            double input2 = Double.parseDouble(txtMilage.getText());
                            double input3 = Double.parseDouble(txtEnginePerformance.getText());
                            double avgInput = (input1+input2+input3)/3;
                            BackwardPropagationAlgorithm b = new BackwardPropagationAlgorithm(input1,input2,input3,avgInput);
                            
                            b.addHiddenInput();
                            b.addOutputLayerInput();
                            for(int i=0;i<300; i++){
                                b.backwardPropagate();
                            }
                            System.out.println("Final Output: "+b.finalOutput);
                            RecommendationDao recDao = new RecommendationDao();
                            RecommendationUI recUI = new RecommendationUI();
                            Object[][] data = recDao.getCarRecommendation(b.finalOutput);           
                            
                            recUI.init(data);
    		   }
       	}
       }
       );
       
       JLabel blank2 = new JLabel(" ");
       btnPanel.add(blank1);
       btnPanel.add(submit);
       btnPanel.add(blank2);
       
       
       
       add(reqPanel,BorderLayout.NORTH);
       add(featurePanel1,BorderLayout.WEST);
       add(featurePanel2,BorderLayout.EAST);
       add(btnPanel,BorderLayout.SOUTH);
         
   }
    
   
   
  public static void main(String []args)
    {
    }
    
}