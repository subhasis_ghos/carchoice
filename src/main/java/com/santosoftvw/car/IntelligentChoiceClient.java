package com.santosoftvw.car;

import java.awt.Dimension;

import com.santosoftvw.car.AdministrationInterface;
import com.santosoftvw.car.CarInterface;
import com.santosoftvw.car.ConfigurationInterface;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

class IntelligentChoiceClient {

    /*public IntelligentChoiceClient()
    {
    	
    }*/

    public static void main(String []args)
    {
    	AdministrationInterface administrationInterface = new  AdministrationInterface();
    	CarInterface carInterface = new CarInterface();
    	ConfigurationInterface configurationInterface = new ConfigurationInterface();
        //RecommendationUI recommendationUI = new RecommendationUI();
        //recommendationUI.init();
	    JFrame jFrame=new JFrame();
	    jFrame.setTitle("Intelichoice");
	    jFrame.getToolkit();
	    Dimension size = jFrame.getToolkit().getScreenSize();
	    jFrame.setBounds(size.width/3,size.height/3,400,300);        
	    jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    JTabbedPane tabbedPane = new JTabbedPane();
	    tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
	    tabbedPane.addTab("User", carInterface);
	    tabbedPane.addTab("Adminstration page", administrationInterface);
	    
	    tabbedPane.addTab("Car Configuration", configurationInterface);
            //tabbedPane.addTab("Recommendation", recommendationUI);
	    jFrame.add(tabbedPane);
	    jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    jFrame.setVisible(true);
	    jFrame.setResizable(false);
	    
        }

}