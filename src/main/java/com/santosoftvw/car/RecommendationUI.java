/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santosoftvw.car;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;


public class RecommendationUI extends JPanel {

    final String[] colHeads={"Model No", "Color", "Sit Capacity", "Fuel Tank Capacity",
                            "Transmission", "Body Style", "Millage", "Engine Performance", "Price"};
    
    Object[][] data1 = {{"2000","RED","4","2000CC","Hybrid","Sedan","20km/h","200HP","TK1,00,000"}};
    
    JTable table;
    
    public RecommendationUI(){
        super();
    }
    
    public void init(Object[][] data){
        setLayout(new BorderLayout());
        
        table = new JTable(data, colHeads);
        
        int v = ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
        int h = ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
        
        JScrollPane jsp = new JScrollPane(table,v,h);
        
        add(jsp,BorderLayout.CENTER);
        
        JFrame jFrame = new JFrame();
        jFrame.add(this);
        jFrame.setVisible(true);
        //jFrame.setSize(850,600);
        jFrame.setBounds(100,50,850,600);
        
    }
    
    
    
    
    
}
